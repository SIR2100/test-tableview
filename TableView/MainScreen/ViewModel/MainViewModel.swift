//
//  MainViewModel.swift
//  TableView
//
//  Created by SOROKANYUK Igor on 05/06/2019.
//  Copyright © 2019 SOROKANYUK Igor. All rights reserved.
//

import Foundation

protocol MainViewModelProtocol: AnyObject {
    func presentationModel(_ model: MainViewModel, newState: MainViewModel.State)
}

class MainViewModel {
    enum State {
        case initial
        case reloading
        case lazyLoading
        case reloadingSucc
        case lazyLoadingSucc
        case reloadingFail(NSError?)
        case lazyLoadingFail(NSError?)
    }

    // MARK: - Property

    public weak var delegate: MainViewModelProtocol?

    private(set) var transactions: [Transaction]?
    private var isLastPage = false
    private var page = 0
    private var countOfTransactionsOnPage = 30
    private var transactionsService: TransactionsServiceProtocol! = TransactionsService() // По хорошему должно собираться DI
    public var state = State.initial {
        didSet {
            // В данном случае сложно повесить слежение на данную переменную (или транзакции) из вне, потому View Model не является пассивной
            delegate?.presentationModel(self, newState: state)
        }
    }
}

// MARK: - Actions
extension MainViewModel {

    func loadTransactions(cardId: String) {
        transactionsService.getTransactions(cardId: cardId, page: page, count: countOfTransactionsOnPage) { response in

            switch response {
            case .success(let transactions):
                self.transactions = transactions
                if transactions.count < self.countOfTransactionsOnPage { self.isLastPage = true }

                switch self.state {
                case .reloading: self.state = .reloadingSucc
                case .lazyLoading: self.state = .lazyLoadingSucc
                default: break
                }
            case .failure(let error):
                switch self.state {
                case .reloading: self.state = .reloadingFail(error)
                case .lazyLoading: self.state = .lazyLoadingFail(error)
                default: break
                }
            }
        }
    }

    func reloadTransactions(cardId: String) {
        transactions = []
        isLastPage = false
        page = 0
        state = .reloading
        loadTransactions(cardId: cardId)
    }
}

