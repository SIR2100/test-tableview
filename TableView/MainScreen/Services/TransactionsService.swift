//
//  TransactionsService.swift
//  TableView
//
//  Created by SOROKANYUK Igor on 06/06/2019.
//  Copyright © 2019 SOROKANYUK Igor. All rights reserved.
//

import Foundation

protocol TransactionsServiceProtocol: AnyObject {
    func getTransactions(cardId: String, page: Int, count: Int, completion: @escaping (Result<[Transaction], NSError>) -> Void)
}

class TransactionsService: TransactionsServiceProtocol {
    func getTransactions(cardId: String, page: Int, count: Int, completion: @escaping (Result<[Transaction], NSError>) -> Void) {
        // Do something here
    }
}
