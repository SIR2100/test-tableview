//
//  MainScreenViewController.swift
//  TableView
//
//  Created by SOROKANYUK Igor on 05/06/2019.
//  Copyright © 2019 SOROKANYUK Igor. All rights reserved.
//

import UIKit

class MainScreenViewController: UIViewController {
    
    @IBOutlet private var someTableView: UITableView! {
        didSet {
            let nib = UINib(nibName: Constants.cellName, bundle: nil)
            someTableView.register(nib, forCellReuseIdentifier: Constants.cellName)
            someTableView.estimatedRowHeight = Constants.estimatedRowHeight
            someTableView.rowHeight = UITableView.automaticDimension
            someTableView.delegate = self
            someTableView.dataSource = self

            someTableView.refreshControl = refreshControl
        }
    }

    fileprivate lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refreshtableView), for: .valueChanged)
        return refreshControl
    }()

    var viewModel = MainViewModel()
    var currentCardId: String = "12345"

    override func viewDidLoad() {
        super.viewDidLoad()
        self.configure()
    }

    private func configure() {
        viewModel.delegate = self
    }
}

// MARK: - MainViewModelDelegate
extension MainScreenViewController: MainViewModelProtocol {
    func presentationModel(_ model: MainViewModel, newState: MainViewModel.State) {
        self.viewModel = model
        self.updateTableView(for: newState)
    }
}

// MARK: - TableView delegates
extension MainScreenViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let transactionsCount = viewModel.transactions?.count else { return 0 }
        
        return transactionsCount
    }

    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: UITableViewCell!
        switch viewModel.state {
        case .initial, .reloadingFail:
            cell = UITableViewCell()
        case .reloading:
            let cellIdentifier = String(describing: LoadingIndicatorCell.self)
            cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath)
        case .reloadingSucc, .lazyLoading, .lazyLoadingSucc, .lazyLoadingFail:
            let cellIdentifier = String(describing: TransactionCell.self)
            cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath)
            (cell as! TransactionCell).configure(from: viewModel.transactions![indexPath.row])
        }
        return cell
    }
}

// MARK: - Logic
extension MainScreenViewController {
    @objc private func refreshtableView() {
        viewModel.reloadTransactions(cardId: currentCardId)
    }

    private func updateTableView(for state: MainViewModel.State) {
        switch state {
        case .reloading:
            someTableView.reloadData()
        case .reloadingSucc:
            someTableView.reloadData()
        case .lazyLoadingSucc:
            someTableView.beginUpdates()
            // Работа с новыми IndexPath
            someTableView.endUpdates()

        case let .reloadingFail(error):
            guard let error = error else { return }
            someTableView.reloadData()
            someTableView.setContentOffset(.zero, animated: true)
            // Обработка ошибок
        case let .lazyLoadingFail(error):
            guard let error = error else { return }
            // Обработка ошибок
        default:
            break
            //Отображаем пользователю, что таблица пуста
        }
    }

}

// MARK: - Constants
fileprivate extension MainScreenViewController {
    struct Constants {
        static let storyboardName = "Main"
        static let cellName = "\(TransactionCell.self)"
        static let estimatedRowHeight: CGFloat = 44.0
    }
}
