//
//  MainModel.swift
//  TableView
//
//  Created by SOROKANYUK Igor on 05/06/2019.
//  Copyright © 2019 SOROKANYUK Igor. All rights reserved.
//

import Foundation

struct Transaction {
    let cardNumber: String
    let date: Date
    let amount: String
    let currency: String
    let operationText: String

    init(cardNumber: String, date: Date, amount: String, currency: String, operationText: String) {
        self.cardNumber = cardNumber
        self.date = date
        self.amount = amount
        self.currency = currency
        self.operationText = operationText
    }
}
