//
//  TransactionCell.swift
//  TableView
//
//  Created by SOROKANYUK Igor on 06/06/2019.
//  Copyright © 2019 SOROKANYUK Igor. All rights reserved.
//

import UIKit

class TransactionCell: UITableViewCell {
    
    @IBOutlet private var descriptionLabel: UILabel!
    @IBOutlet private var valueLabel: UILabel!

    // MARK: - Lifecycle

    override func prepareForReuse() {
        super.prepareForReuse()
        descriptionLabel.text = nil
        valueLabel.text = nil
    }

    // MARK: - Configure

    func configure(from transaction: Transaction) {
        descriptionLabel.text = transaction.operationText
        valueLabel.text = "\(transaction.amount)"
    }
}
